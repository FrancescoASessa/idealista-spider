# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
import re

class ListatoSpider(scrapy.Spider):
    name = 'listato'
    allowed_domains = ['www.idealista.it']
    start_urls = ['https://www.idealista.it/affitto-case/catanzaro-provincia/']

    def modify_realtime_request(self, request):
        return request

    def parse(self, response):
        # Container:    .items-container
        # Item:         article.item-multimedia-container
        # Image:        //picture[contains(@class, "item-multimedia ")]//img/@src
        # Info:         div.item-info-container
        # Link:         a.item-link 
        # Title:        a.item-link title attribute
        # Price:        spam.item-price
        # Description:  div.item-description > .ellipsis
        for item in response.css("article.item-multimedia-container"):
            title = item.css('a.item-link').xpath("text()").extract_first()
            linkHtml = item.css('a.item-link').xpath("@href").extract_first()
            link = "https://www.idealista.it{}".format(linkHtml)
            image = item.xpath('//picture[contains(@class, "item-multimedia ")]//img/@src').extract_first()
            priceHtml = item.xpath('//span[contains(@class, "item-price")]').extract_first()
            price = self.remove_tags(priceHtml)
            descriptionHtml = item.css('div.item-description').get()
            description = self.remove_tags(descriptionHtml)
            yield {
                'title': title,
                'link': link,
                'price': price,
                'description': description,
                'image': image
            }
        pass
    def remove_tags(self,text):
        return re.compile(r'<[^>]+>').sub('', text)